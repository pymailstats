#!/usr/bin/env python

import email
import email.utils
from time import *
from email.utils import parseaddr
from mailclient import *

class basedata:
	def __init__(self, initMail):
		self.date = email.utils.parsedate_tz(initMail.get("Date"))
		self.mailer =  (initMail.get("X-Mailer") or
				initMail.get("User-Agent") or
				initMail.get("X-MimeOLE"))

		if self.mailer:
			self.mailer = mailClient(self.mailer)
		else:
			# There are bad people outside, which don't
			# add some mailer information to header.
			if (initMail.get("Sender") == "gmail.com" or
			    initMail.get("From").find("google") != -1 or
			    (initMail.get("Message-ID") 
			     and initMail.get("Message-ID").find("@gmail.com")
			     != -1)):
				self.mailer = mailClient("Google")
			else:
				self.mailer = mailClient("Unknown")

		self.mail_from = parseaddr(initMail.get("From"))

	def __cmp___(self, other):
		return 0

	def getAllDate(self):
		if self.date:
			return self.date
		else:
			return None

	def getYear(self):
		if self.date:
			return self.date[0]
		else:
			return None

	def getMonth(self):
		if self.date:
			return self.date[1]
		else:
			return None

	def getDay(self):
		if self.date:
			return self.date[2]
		else:
			return None

	def getDayName(self):
		return None

	def getHour(self):
		if self.date:
			return self.date[3]
		else:
			return None

	def getMinute(self):
		if self.date:
			return self.date[4]
		else:
			return None

	def getSeconds(self):
		if self.date:
			return self.date[5]
		else:
			return None

	def getDate(self):
		return self.getYear(), self.getMonth(), self.getDay()

	def getMailer(self):
		return self.mailer.getMailClient()

	def getPureMailer(self):
		return self.mailer.mailclient

	def getMakeTm(self):
		if self.date:
			return email.utils.mktime_tz(self.date)
		else:
			return email.utils.mktime_tz(0)

	def getFromMailAddr(self):
		return self.mail_from[1]
