#!/usr/bin/env python

from mailbasedata import *
import email
import mhlib

# Global variable
mh = mhlib.MH("")
dir = mhlib.Folder(mh, "")
mails = []

# basic functions
def parseMails():
	for i in dir.listmessages():
		mails.append(basedata(mhlib.Message(dir, i)))


"""Returns a dictionary with all acitve senders (mail address) as key
and the number of mails as value. If there is no mail, the dictonary
will be empty."""
def getActivePoster():
	poster = {}
	for i in mails:
		if i.getFromMailAddr() == None:
			print i
		if i.getFromMailAddr() in poster:
			poster[i.getFromMailAddr()] = poster[i.getFromMailAddr()] + 1
		else:
			poster[i.getFromMailAddr()] = 1
	return poster


def getUserMailClient():
	poster = {}
	foo = []
	for i in mails:
		if i.getPureMailer() == None:
			print i
		if i.getPureMailer() in poster:
			poster[i.getPureMailer()] = poster[i.getPureMailer()] + 1
		else:
			poster[i.getPureMailer()] = 1
	for i,j in poster.iteritems():
		foo.append([i, j])
	foo.sort(key=lambda t:-t[1])
	for i in foo:
		print i

"""Returns a dictionary with a tupel mail address, and mail client as
key and the number of reconized mails as value.  If there is no mail
recognized, it will return an empty dictionary. Combninations from
mail clinet and email address that did not occur, will have no entry
inside the dict."""
def getMailClientPerAddress():
	pairs = {}
	foo = []
	for i in mails:
		if ((i.getFromMailAddr(), i.getMailer()) in pairs):
			pairs[i.getFromMailAddr(), i.getMailer()] = pairs[i.getFromMailAddr(), i.getMailer()] +1
		else:
			pairs[i.getFromMailAddr(), i.getMailer()] = 1

	return pairs

"""Returns a dictionary with procentage of usage of each recognized
mail client for an email address. The key is builded from trupel of
email address and mailclient and will be empty if no mail is
recognized."""
def getUserFavoriteMailer():
	# getting data we need here
	client_per_address = getMailClientPerAddress()
	mails_per_poster = getActivePoster()

	usage = {}
	tmp = client_per_address.keys()
	for i,j in tmp:
		usage[i,j] = client_per_address[i,j] * 100 // mails_per_poster[i]
		print i, j, usage[i,j]


def getMailPerDayOfWeek():
	return None


def getJustEveryMailBasisDate():
	for i in mails:
		print i.getMailer(), "\t",
		print i.getDate()
		print i.getFromMailAddr()


parseMails()
foo = getActivePoster()
for i in foo:
	print i, foo[i]
print getUserFavoriteMailer()
print getUserMailClient()
foo = getMailClientPerAddress()
baa = foo.keys()
for i in baa:
	print i,foo[i]
for i in mails:
	print i.getFromMailAddr(),
	print i.getMailer()
