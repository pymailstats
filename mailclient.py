#!/usr/bin/env python

class mailClient:

	def __init__(self, mailerstring):
		# The following part needs to being moved
		# to another structure. However. For the beginning it
		# should work like this ... Or not ;)

		tmp = mailerstring.split()
		# setting default value to ensure we got some client
		# in case no of the following rules apply
		self.OS = None
		self.mailclient = mailerstring
		self.version = None
		self.buildstr = None
		if tmp[0] == "Sylpheed" and tmp[0] != mailerstring:
			if tmp[1] == "version":
				self.mailclient = tmp[0]
				self.version = tmp[2]
				self.buildstr = None
			else:
				self.mailclient = tmp[0]
				self.version = tmp[1]
				self.buildstr = tmp[2]
		if tmp[0].startswith("Claws"):
			self.mailclient = "Claws Mail"
			self.version = tmp[2]
		if (tmp[0] == "Thunderbird" or
			tmp[0] == "Mozilla-Thunderbird" or
			tmp[0] == "IceDove"):
			self.mailclient = "Thunderbird"
			self.version = tmp[1]
			self.buildstr = tmp[2]
		if tmp[0].startswith( "Mozilla/5.0"):
			self.mailclient = "Mozilla based"
			self.version = None
			self.buildstr = None
		if (tmp[0].startswith("Mutt") or
			tmp[0].startswith("SquirrelMail") or
		   	tmp[0].startswith("KMail") or
			tmp[0].startswith("KNode") or
			tmp[0].startswith("Gnus") or
			tmp[0].startswith("tin") or
			tmp[0].startswith("slrn") or
			tmp[0].startswith("Pan")):
			if tmp[0].find("/") > -1:
				self.mailclient, self.version = tmp[0].split("/")
				if self.version.find("+") > -1:
					self.version, self.buildstr = self.version.split("+")
		# Mailclient with single Name and simple version string
		# without buildinfo
		if tmp[0] == "Balsa" or tmp[0] == "Evolution":
			self.mailclient = tmp[0]
			self.version = tmp[1]
		# Mailclient with single Name, version stirng and build string
		if tmp[0] == "Alpine":
			self.mailclient = tmp[0]
			self.version = tmp[1]
			self.buildstr = tmp[2]

	def __cmp__(self, other):
		return 0

	def getOS(self):
		"""Not yet implemented"""
		return self.OS

#	def getMailClient(self):
#		return self.mailclient, return self.version

	def getVersion(self):
		return self.version

	def getBuildString(self):
		return self.buildstr

	def getMailClient(self):
	 	return self.mailclient, self.version, self.buildstr, self.OS


